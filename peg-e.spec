Name:           peg-e
Version:        1.3.0
Release:        1%{?dist}
Summary:        Peg solitaire game
License:        GPLv3+
URL:            http://gottcode.org/%{name}/
Source0:        http://gottcode.org/%{name}/%{name}-%{version}-src.tar.bz2

BuildRequires:  desktop-file-utils
BuildRequires:  libappstream-glib
BuildRequires:  qt5-qttools-devel

Requires:       hicolor-icon-theme

%description
Peg-E is a peg solitaire game in which you jump over pieces in order to
remove them from the board, ultimately trying to eliminate all but one.
The boards are randomly generated, with 100 levels of difficulty. The game
auto-saves, and has undo-redo capability. Pieces can move horizontally,
vertically, and diagonally.

%prep
%setup -q

%build
%{qmake_qt5} PREFIX=%{_prefix}
make %{?_smp_mflags}

%install
make install INSTALL_ROOT=%{buildroot}

%find_lang pege --with-qt --without-mo

%check
desktop-file-validate %{buildroot}/%{_datadir}/applications/%{name}.desktop || :
appstream-util validate-relax --nonet %{buildroot}%{_datadir}/metainfo/%{name}.appdata.xml || :

%files -f pege.lang
%doc ChangeLog CREDITS README
%license COPYING
%{_bindir}/%{name}
%{_datadir}/metainfo/%{name}.appdata.xml
%{_datadir}/applications/%{name}.desktop
%dir %{_datadir}/%{name}/
%{_datadir}/%{name}/icons/
%dir %{_datadir}/%{name}/translations
%{_datadir}/icons/hicolor/*/apps/%{name}.*
%{_mandir}/man6/%{name}.6.*


%changelog
* Mon Nov 29 2021 Yaakov Selkowitz <yselkowi@redhat.com> - 1.3.0-1
- new version

* Fri Jul 31 2020 Yaakov Selkowitz <yselkowi@redhat.com> - 1.2.8-1
- Initial package
